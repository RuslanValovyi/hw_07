#include <iostream>
#include <functional>
#define countof(array) (sizeof(array)/sizeof(*(array)))

using namespace std;

int add(int a, int b)
{
	return static_cast<int>(a + b);
}

int divide(int a, int b)
{
	return static_cast<int>(a / b);
}

int multiply(int a, int b)
{
	return static_cast<int>(a * b);
}

int subtract(int a, int b)
{
	return static_cast<int>(a - b);
}

using func_t = function<int(int,int)>;

struct {
    char operation;
    func_t func;
} const operations[] {
	{'+', add},
	{'-', subtract},
	{'/', divide},
	{'*', multiply}
};

struct {
	int a;
	char operation;
	int b;
} const input_data[] = {
	{10, '+',  5},
	{18, '/',  3},
	{ 6, '*',  9},
	{ 9, '-', 10}
};

int main() {
	for (int i=0; i < countof(input_data); i++) {
		for (int n=0; n < countof(operations); n++) {
			if (input_data[i].operation == operations[n].operation) {
				cout << input_data[i].a << input_data[i].operation << input_data[i].b << "=" << operations[n].func(input_data[i].a, input_data[i].b) << endl;
			}
		}
	}

	return 0;
}
