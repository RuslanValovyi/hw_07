homework is a bit different from today's lecture because this topic will be covered in the next homework. For now please do next:

Create following functions for several arithmetical operations:
int add(int, int)
int divide(int, int)
int multiply(int int)
int subtract(int, int)

using 'using' or 'typedef' or std::function to get pointer (let's name it func_t)
to any of these functions, create following structure:

struct operation_t {
    char operation;
    func_t func;
};

also create an array which matches operations with corresponding functions:
operation_t operations[] {
... < insert your values here >
};

then iterate over this array and call each function with given arguments 
and print the results
